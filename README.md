# DevOps-Case2

## First command
sudo ansible-playbook -i inventory.cfg nginx_install.yml -b

PLAY [web] **********************************************************************************************************

TASK [Gathering Facts] **********************************************************************************************
ok: [127.0.0.1]

TASK [ensure nginx is at the latest version] ************************************************************************
ok: [127.0.0.1]

TASK [start nginx] **************************************************************************************************
ok: [127.0.0.1]

PLAY RECAP **********************************************************************************************************
127.0.0.1                  : ok=3    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0  

## Second command
sudo ansible-playbook -i inventory.cfg  --limit 127.0.0.1 nginx.yml

PLAY [web] **********************************************************************************************************

TASK [Gathering Facts] **********************************************************************************************
ok: [127.0.0.1]

TASK [ensure nginx is at the latest version] ************************************************************************
ok: [127.0.0.1]

TASK [start nginx] **************************************************************************************************
ok: [127.0.0.1]

TASK [copy the nginx config file and restart nginx] *****************************************************************
changed: [127.0.0.1]

TASK [create symlink] ***********************************************************************************************
changed: [127.0.0.1]

TASK [copy the content of the web site] *****************************************************************************
ok: [127.0.0.1]

TASK [restart nginx] ************************************************************************************************
changed: [127.0.0.1]

PLAY RECAP **********************************************************************************************************
127.0.0.1                  : ok=7    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
 
## Third command
curl http://127.0.0.1

<h1>Welcome to Ansible</h1>
<img src="./ansible-logo.jpg" />






